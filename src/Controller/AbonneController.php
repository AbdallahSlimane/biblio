<?php

namespace App\Controller;

use App\Entity\Abonne;
use App\Entity\Emprunt;
use App\Entity\Livre;
use App\Form\AbonneType;
use App\Form\FormLivreType;
use App\Repository\AbonneRepository;
use App\Repository\EmpruntRepository;
use App\Repository\LivreRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;




class AbonneController extends AbstractController
{
    /**
     * @Route("/abonne", name="abonne_index", methods={"GET"})
     */
    public function index(AbonneRepository $abonneRepository): Response
    {
        return $this->render('abonne/index.html.twig', [
            'abonnes' => $abonneRepository->findAll(),
        ]);
    }

    /**
     * @Route("/abonne/nouveau", name="abonne_new", methods={"GET","POST"})
     */
    public function new(UserPasswordEncoderInterface $passwordEncoder, Request $request): Response
    {
        $abonne = new Abonne();
        $form = $this->createForm(AbonneType::class, $abonne);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $abonne->setPassword(
                $passwordEncoder->encodePassword(
                    $abonne,
                    $form->get('password')->getData()
                )
            );
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($abonne);
            $entityManager->flush();

            return $this->redirectToRoute('abonne_index');
        }

        return $this->render('abonne/new.html.twig', [
            'abonne' => $abonne,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/abonne/{id}", name="abonne_show", methods={"GET"})
     */
    public function show(Abonne $abonne): Response
    {
        return $this->render('abonne/show.html.twig', [
            'abonne' => $abonne,
        ]);
    }

    /**
     * @Route("/abonne/{id}/modifier", name="abonne_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Abonne $abonne): Response
    {
        $form = $this->createForm(AbonneType::class, $abonne);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('abonne_index');
        }

        return $this->render('abonne/edit.html.twig', [
            'abonne' => $abonne,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/abonne/{id}", name="abonne_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Abonne $abonne): Response
    {
        if ($this->isCsrfTokenValid('delete' . $abonne->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($abonne);
            $entityManager->flush();
        }

        return $this->redirectToRoute('abonne_index');
    }

    /**
     * @Route("/profil", name="profil", methods={"GET"})
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function profil(): Response
    {

        return $this->render('abonne/profil.html.twig', []);
    }
    /**
     * @Route("/profil/reserver", name="reserver", methods={"GET","POST"})
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function reserver(LivreRepository $livreRepository, EmpruntRepository $empruntRepository, Request $request, EntityManagerInterface $manager): Response
    {
        $liste_livres = $livreRepository->findAll();
        $empruntLivreNonRendus = $empruntRepository->findByNonEmprunte();
        $livresRendu = [];

        // On va parcourir la liste de tous les livres.  
        //Pour chaque livre, on va chercher à savoir s'il fait partie des livres non rendus.
        //Si il ne fait pas partie des livres non rendus, je l'ajoute à l'array $livreRendu

        // On parcourt tous les livres de la bibliothèque
        foreach ($liste_livres as $livre) {
            // je crée une variable booléenne qui sera vrai si le livre actuel est non rendus
            $nonRendu = false;


            //POur chaque livre, on parcourt les livres non rendus
            foreach ($empruntLivreNonRendus as $emprunt) {
                //On compare le livre actuel avec le livreNonRendu
                if ($livre->getId() == $emprunt->getLivre()->getId()) {
                    //Si l'id du livre est égale à l'id livre non rendu, alors $nonRendu est vrai
                    $nonRendu = true;
                }
            }

            //Si $nonRendu est faux, on ajoute le livre à l'array des livres rendus
            if (!$nonRendu) {
                $livresRendu[] = $livre;
            }
        }

        //isMethod() permet de savoir si ma requete HTTP est en GET ou en POST
        if ($request->isMethod("POST")) {
            // La propriété request de l'objet $request permet de récupérer ce qu'il y a dans $_POST
            $liste_livres = $request->get("liste_livres");
            if ($liste_livres) {
                foreach ($liste_livres as $id_livre) {
                    $emprunt = new Emprunt;
                    $emprunt->setAbonne($this->getUser());
                    $emprunt->setLivre($livreRepository->find($id_livre));
                    $emprunt->setDateSortie(new \DateTime('now'));
                    $manager->persist($emprunt);
                }
                $manager->flush();
                return $this->redirectToRoute('profil');
            }
        }
        return $this->render('abonne/reservation.html.twig', [
            'livresRendu' => $livresRendu
        ]);
    }
    /**
     * @Route("profil/ajouter-livre", name="livre_ajouter", methods={"GET","POST"})
     */
    public function ajouterLivre(LivreRepository $livreRepository, EntityManagerInterface $manager,  Request $request)
    {
        $nouveauLivre = new Livre;
        $formAjouter = $this->createForm(FormLivreType::class, $nouveauLivre);
        $formAjouter->handleRequest($request);
        if ($formAjouter->isSubmitted()) {
            if ($formAjouter->isValid()) {
                $manager->persist($nouveauLivre);
                $manager->flush();


                $this->addFlash('success', '<i>' . $nouveauLivre->getTitre() . ' </i> a été ajouté');
                return $this->redirectToRoute('acceuil');
            } else {
                $this->addFlash('warning', 'Le formulaire n\'est pas valide');
            }
        }
        $formAjouter = $formAjouter->createView();
        return $this->render('livre/form_ajouter.html.twig', [
            'formAjouter' => $formAjouter
        ]);
    }
}
